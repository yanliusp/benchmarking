#!/bin/bash

# stop when error
set -e

# set series number and config files to work with
#series_num=01140716_0726
#processing_config=processingSoudanData.R134Grid.z14lite.bg_cf
#processing_config=processingSoudanDataTutorialVersion.R134Grid.z14lite.bg_cf
#analysis_config=configSoudanData.R134.z14lite
#max_events=10000

#example
#series_num=23190928_1623
#processing_config=processingCUTEData.G124
#analysis_config=configCUTEData.G124
#max_number_dumps=3
#max_events=200

# CUTE Ba R19
#series_num=23210107_180316
#processing_config=processingCUTEData.R19.DataType10
#analysis_config=configCUTEData.R19.DataType10
#max_number_dumps=2
#max_events=10000
# BatNoise -s 23210107_180316 -d 1 --max_events 200 --processing_config processingCUTEData.R19.DataType10 --analysis_config configCUTEData.R19.DataType10

#TRIUMF picoscope hybrid
#series_num=21210907_152363 # or, 21210907_152366
#series_num=21210907_152366 # or, 21210907_152366
#processing_config=processingHybridData.Default_MDItest
#analysis_config=configHybridData.Default_MDItest
#max_number_dumps=20
#max_events=10000

# SLAC hybrid data
#series_num=09211008_112921
#processing_config=processingSLACHybridData.R85
#analysis_config=configSLACHybridData.R85
#processing_config=processingHybridData.General
#analysis_config=configHybridData.General_2
#max_events=10000
#max_events=60

# Bruno SLAC fake hybrid data
#series_num=51221114_143726
#processing_config=processingSLACHybridData.R85
#analysis_config=configSLACHybridData.R85
#max_events=10000

# Bruno SLAC fake hybrid data
#series_num=51221115_155908
#processing_config=processingHybridData.General
#analysis_config=configHybridData.General_2
#max_events=10000

# Yan Elias fake hybrid data
#random   physics    series number
#  100      500     51221118_221100
#  200      400     51221118_220814
#  300      300     51221118_220528
#  400      200     51221118_220236
#  500      100     51221118_215901
#series_num=51221118_221100
#series_num=51221118_220814
#series_num=51221118_220528
#series_num=51221118_220236
#series_num=51221118_215901
#processing_config=processingHybridData.General
#analysis_config=configHybridData.General_2
#max_events=10000

# Yan Elias fake hybrid data
# series number      nb     random  physics
#51221119_182405     100      50      1:1
#51221119_182754      50      25      1:1
#51221119_182928     150      75      1:1
#51221119_183102     200     100      1:1
#51221119_183216     300     150      1:1
#51221119_183331     310     155      1:1
#51221119_183721     400     100      1:3
#51221119_183931     500     100      1:4
#51221119_184108     600     100      1:5
#51221119_184357     700     100      1:6
#51221119_184523     800     100      1:7
#51221119_184644     900     100      1:8
#51221119_184811    1000     100      1:9
#51221119_184938    1100     100     1:10
#series_num=51221119_182405
#series_num=51221119_182928
#series_num=51221119_183102
#series_num=51221119_183216
#series_num=51221119_183331
#series_num=51221119_183721
#series_num=51221119_183931
#series_num=51221119_184108
#series_num=51221119_184357
#series_num=51221119_184523
#series_num=51221119_184644
#series_num=51221119_184811
#series_num=51221119_184938
#processing_config=processingHybridData.General
#analysis_config=configHybridData.General_2
#max_events=10000


# SLAC R85 non-hybrid NxM G133
#series_num=09210915_110633
#processing_config=processingSLACData.SNOLABiZIP
#analysis_config=configSLACData.SNOLABiZIP
#max_events=10000

# Yan Bruno Elias, fake non-hybrid data
#series_num=51221203_171930
#processing_config=processingSLACData.SNOLABiZIP
#analysis_config=configSLACData.SNOLABiZIP
#max_events=10000

# McKay, DAQsim simulated data - HV100mm
#series_num=51221007_000000
##processing_config=processingSLACData.SNOLABiZIP
#processing_config=processingSLACData.SNOLABiZIP_forMckay
#processing_config=processingSLACData.SNOLABHV_forMckay
#analysis_config=configSLACData.SNOLABiZIP
#max_events=10000

# McKay, DAQsim simulated data - iZIP7
#series_num=51221007_000001
#processing_config=processingSLACData.SNOLABiZIP_forMckay
#analysis_config=configSLACData.SNOLABiZIP
#max_events=10000

# Elias's first testNxM files, with delay
#series_num=51221208_004841
#processing_config=processingEliasData.SNOLABHV
#analysis_config=configEliasData.SNOLABHV
#max_events=10000

# Elias' first testNxM files, with delay, second midas version (updated channel mapping, removed noise)
#series_num=51221220_011648  # channel mapping is still wrong
#series_num=51221220_184737 # channel mapping is still wrong
#series_num=51221221_155219 # with delay
#series_num=51221221_212050 # without delay
#processing_config=processingEliasData.SNOLABHV
#analysis_config=configEliasData.SNOLABHV
#max_events=10


# testing analytical delays using Elias' dataset
#series_num=51221221_155219
#processing_config=processingEliasData_analyticalDelay.SNOLABHV
#analysis_config=configEliasData_analyticalDelay.SNOLABHV
#max_events=350


# testing for Lei 2023 Feb. check skype messages for details NEW
#series_num=01140301_0038
#processing_config=processingSoudanData.SuperCDMS.DMC-draft.cdmslite
#analysis_config=configSoudanData.SuperCDMS.DMC-draft.cdmslite
#max_events=10000
# testing for Lei 2023 Feb./Mar. check skype messages for details OLD
#series_num=01140301_0037
#processing_config=processingSoudanData.SuperCDMS.DMC-draft.cdmslite
#analysis_config=configSoudanData.SuperCDMS.DMC-draft.cdmslite
#max_events=10000

# testing 1xMOF implementation, still use some NxM config parameters
# SLAC R85 non-hybrid NxM G133
#series_num=09210915_110633
#processing_config=processingSLACData.SNOLABiZIP_1xM
#analysis_config=configSLACData.SNOLABiZIP_1xM
##max_events=10
#max_events=10000

## testing salting midas writer.
# CUTE Ba R19 data series, MidasDAQ generated
#series_num=23210107_180316
#processing_config=processingCUTEData.R19.DataType10
#analysis_config=configCUTEData.R19.DataType10
#batnoise_dumps=0001-0002
#max_events=10000
# SuperSalt midas writer, IOLibrary generated
#series_num=23210107_180366
#processing_config=processingCUTEData.R19.DataType10
#analysis_config=configCUTEData.R19.DataType10
#batnoise_dumps=0001-0002
#max_events=10000


## testing MDI filter using SLAC hybrid data
#series_num=09211008_112921
#processing_config=processingSLACHybridData.R85_MDI
#analysis_config=configSLACHybridData.R85_MDI
#max_events=10000


## testing DMC HV sample 20230506 Stefan
#series_num=51230427_000001
#processing_config=processingSLACData.SNOLABiZIP_DMCtest
#analysis_config=configSLACData.SNOLABiZIP_DMCtest
#max_events=10000



## testing DMC HV sample 20230510 Mike/Rik
#series_num=10230506_000000
#processing_config=processingSLACData.SNOLABiZIP_DMCtest
#analysis_config=configSLACData.SNOLABiZIP_DMCtest
#max_events=10000


## testing DMC HV sample 20230606, debug for McKay
#series_num=51230509_000002
#processing_config=processingSLACData.SNOLABiZIP_DMCtest
#processing_config=processingMidasData.Default
#analysis_config=configSLACData.SNOLABiZIP_DMCtest
#analysis_config=FuncForm_SNOLAB.G124
#max_events=10000


## testing DMC HV sample (10000 events!) 20230515 Rik
#/sdf/home/y/yanliu/data/HVDMC/51230510_000075
#series_num=51230510_000075
#processing_config=processingSLACData.SNOLABiZIP_DMCtest
#analysis_config=configSLACData.SNOLABiZIP_DMCtest
#max_events=10000
#RAW_DATA_PATH=/sdf/home/y/yanliu/data/HVDMC/51230510_000075


## testing synthetic non-hybrid dataset for Ruchi 20230610 
series_num=51230610_000075
# the following are used for DMC testing and works (?)
#processing_config=processingSLACData.SNOLABiZIP_DMCtest
#analysis_config=configSLACData.SNOLABiZIP_DMCtest
# the following is what ruchi used, and didn't work
processing_config=processingHybridData.Default_MDItest_synthetic_nonhybrid
analysis_config=configHybridData.Default_MDItest_synthetic_nonhybrid
max_events=10000
RAW_DATA_PATH=/sdf/home/y/yanliu/data/ruchi


## testing synthetic hybrid dataset for Ruchi 20230610 
#series_num=51230610_000076
#processing_config=processingHybridData.Default_MDItest
#analysis_config=configHybridData.Default_MDItest
#max_events=10000
#RAW_DATA_PATH=/sdf/home/y/yanliu/data/ruchi



## processing CUTE R33, Ba, to calculate amplification factor
#series_num=23230510_172313 # +/-15V
#series_num=23230510_215240 # +/-5V
#series_num=23230510_225957 # +/-5V
#series_num=23230511_003046 # +/-5V
#series_num=23230511_032625 # +/-5V
#processing_config=processingCUTEData.R33.G124.Ba
#analysis_config=configCUTEData.R33.G124.Ba
#max_events=10000
#RAW_DATA_PATH=/fs/ddn/sdf/group/supercdms/data/CDMS/CUTE/R33/Raw/23230510_172313
#RAW_DATA_PATH=/fs/ddn/sdf/group/supercdms/data/CDMS/CUTE/R33/Raw/23230510_215240
#RAW_DATA_PATH=/fs/ddn/sdf/group/supercdms/data/CDMS/CUTE/R33/Raw/23230510_225957
#RAW_DATA_PATH=/fs/ddn/sdf/group/supercdms/data/CDMS/CUTE/R33/Raw/23230511_032625



# this script will set env variables and paths to executables
export CDMSBATS_DATA="/sdf/home/y/yanliu/data/"
if [[ -z "$RAW_DATA_PATH" ]]; then
	RAW_DATA_PATH=$CDMSBATS_DATA/raw
fi
NOISE_FILE_PATH=$CDMSBATS_DATA/noise
RQ_FILE_PATH=$CDMSBATS_DATA/rq
RRQ_FILE_PATH=$CDMSBATS_DATA/rrq
AUX_FILE_PATH=$CDMSBATS_DATA/aux
GPIB_FILE_PATH=$CDMSBATS_DATA/gpib

#env variables that can be found in tutorial
export CDMSBATSDIR="/sdf/home/y/yanliu/software/cdmsbats/"
export CDMSBATSCONFIGDIR="/sdf/home/y/yanliu/software/cdmsbats_config/"
export BATNOISE_TEMPLATES=$CDMSBATSCONFIGDIR/PulseTemplates
export BATROOT_PROC=$CDMSBATSCONFIGDIR/UserSettings/BatRootSettings/processing
export BATROOT_CONST=$CDMSBATSCONFIGDIR/UserSettings/BatRootSettings/analysis
export BATROOT_RAWDATA=$RAW_DATA_PATH
export BATROOT_NOISEFILES=$NOISE_FILE_PATH
export BATROOT_RQDATA=$RQ_FILE_PATH
export BATCALIB_RQDATA=$RQ_FILE_PATH
export BATCALIB_RRQDATA=$RRQ_FILE_PATH
export BATROOT_AUXFILES=$AUX_FILE_PATH
export BATROOT_GPIBFILES=$GPIB_FILE_PATH

#env variable that were set in install_packages.sh
export CDMS_IOLIBRARY="/sdf/home/y/yanliu/software/IOLibrary/"
export CDMS_BATCOMMON="/sdf/home/y/yanliu/software/BatCommon/"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CDMS_BATCOMMON/BUILD/lib


#set path to executables
export PATH=$PATH:$CDMSBATSDIR/BUILD/bin


BatNoise -s $series_num -d 0001 --max_events $max_events --processing_config $processing_config --analysis_config $analysis_config
#BatNoise -s $series_num -d 0026 --max_events $max_events --processing_config $processing_config --analysis_config $analysis_config
#BatNoise -s $series_num -d 0001-0004 --processing_config $processing_config --analysis_config $analysis_config
#BatNoise -s $series_num -d 0001 --processing_config $processing_config --analysis_config $analysis_config # 1xMOF validation
#BatNoise -s $series_num -d 0001 --processing_config $processing_config --analysis_config $analysis_config # MDIOF validation
#BatNoise -s $series_num -d $batnoise_dumps --processing_config $processing_config --analysis_config $analysis_config # SuperSalt validation
#run BatRoot
#for run in {1..1}
#for run in {2..2}
#for run in {2..2}
#for run in {1..219}
#for run in {60..60}
for run in {26..26}
#for run in {1..26}
do
  echo $run
  BatRoot -s $series_num -d $run --max_events $max_events --processing_config $processing_config --analysis_config $analysis_config
done
