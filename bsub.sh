#!/bin/bash
#####SBATCH --time=48:00:00
#####SBATCH --partition=roma
#####SBATCH --mem-per-cpu=4096M
#####SBATCH --job-name=RootBenchmark-job
#####SBATCH --mail-type=ALL
#####SBATCH --mail-user=yanliusp@gmail.com
#####SBATCH -o JOB%j.out # File to which STDOUT will be written
#####SBATCH -e JOB%j.err # File to which STDERR will be written


#max number of times to repeat job
#benchmarking BatRoot
#for i in {1..40} #do same dumps inside testRoot multiple times
for i in {1..1} #do same dumps inside testRoot multiple times
do
  #LOGFILE="Singularity_exec_BatNoise_${SLURM_JOB_ID}_1th_iteration.log" #new log file for each run
  #LOGFILE="Singularity_test_${SLURM_JOB_ID}.log" #new log file for each run
  WALL_START=$(date +%s.%N)
  #srun --output=${LOGFILE} singularity exec -B /sdf/home/y/yanliu/,/sdf/scratch/yanliu/ /sdf/home/y/yanliu/singularity/cdmsbaseana_ubuntu20.04_root6.24.06.sif bash /sdf/home/y/yanliu/software/benchmarking/testRoot.sh #bash needed to use loop index in testRoot
  #singularity exec -B /sdf/home/y/yanliu/,/sdf/scratch/yanliu/ /sdf/home/y/yanliu/singularity/cdmsbaseana_ubuntu20.04_root6.24.06.sif bash /sdf/home/y/yanliu/software/benchmarking/testRoot_update.sh #bash needed to use loop index in testRoot
  #singularity exec -B /sdf/home/y/yanliu/,/sdf/scratch/yanliu/ /sdf/home/y/yanliu/singularity/cdmsbaseana_ubuntu20.04_root6.24.06.sif bash /sdf/home/y/yanliu/software/benchmarking/testRoot.sh #bash needed to use loop index in testRoot
  #singularity exec -B /sdf/home/y/yanliu/,/sdf/scratch/yanliu/ /sdf/home/y/yanliu/singularity/cdmsbaseana_ubuntu20.04_root6.24.06.sif bash /sdf/home/y/yanliu/software/benchmarking/testRoot_all_1.sh #bash needed to use loop index in testRoot
  singularity exec -B /sdf/home/y/yanliu/,/sdf/scratch/yanliu/ /sdf/home/y/yanliu/singularity/cdmsbaseana_ubuntu20.04_root6.24.06.sif bash /sdf/home/y/yanliu/software/benchmarking/testRoot.sh #bash needed to use loop index in testRoot
  #singularity exec -B /sdf/home/y/yanliu/,/sdf/scratch/yanliu/,/fs/ddn/sdf/group/supercdms/data/CDMS/CUTE/R33/Raw/23230511_032625 /sdf/home/y/yanliu/singularity/cdmsbaseana_ubuntu20.04_root6.24.06.sif bash /sdf/home/y/yanliu/software/benchmarking/testRoot.sh #process CUTE R33 Ba data
  echo "slurm_job_id is: " $SLURM_JOB_ID
  WALL_END=$(date +%s.%N)
  WALL_RUNTIME=$(echo "$WALL_END - $WALL_START" | bc -l)
  echo "Wall time is: " $WALL_END $WALL_START $WALL_RUNTIME
  echo "$WALL_END - $WALL_START = $WALL_RUNTIME " >> /sdf/home/y/yanliu/software/benchmarking/output/Wall_time_BatRoot_$series_num.txt

done
cd ~
