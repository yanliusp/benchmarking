# benchmarking

Some scripts used for benchmarking CDMSBats process software on s3df using slurm.

## Usage

To submit a job to s3df, run
`srun bsub.sh` or 
`sbatch bsub.sh`
