// calculate and plot the benchmarking results for non-hybrid data

#include <iostream>
#include <fstream>
#include <numeric>

struct data_t {         /* simple struct to coordinate data for each city */
    int dump;
    std::string process;
    double start, end;
};

struct plot_t {
    double mean;
    double stdev;
};

plot_t calcFile (std::string fileName) {

   ifstream txtfile (fileName);

   std::string line;
   std::vector<double> duration;
   plot_t ptmp;


   while (getline(txtfile, line)){
	   data_t dtmp;
	   std::string stmp;
	   std::stringstream ss (line);

	   ss >> dtmp.process;
	   ss >> dtmp.dump;
	   ss >> dtmp.end;
	   getline (ss, stmp, '-');
	   ss >> dtmp.start;
	   duration.push_back(dtmp.end - dtmp.start);
   }

   double sum = std::accumulate(duration.begin(), duration.end(), 0.0);
   double mean = sum / duration.size();
   double sq_sum = std::inner_product(duration.begin(), duration.end(), duration.begin(), 0.0);
   double stdev = std::sqrt(sq_sum / duration.size() - mean * mean);
   std::cout << "The average time is " << mean << ", and standard deviation is " << stdev << std::endl;
   ptmp.mean = mean;
   ptmp.stdev = stdev;

   return ptmp;
}

void linear_fit_eventno(std::vector<plot_t> pvec1, std::vector<plot_t> pvec2, std::vector<plot_t> pvec3) {

   const int iPoints = 12; //hardcode
   double x[iPoints] = {100, 150, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100}; //hardcode
   double xe[iPoints] = {0, 0, 0, 0,
                         0, 0, 0, 0,
                         0, 0, 0, 0};
   double y[iPoints], y2[iPoints], y3[iPoints];
   double ye[iPoints], y2e[iPoints], y3e[iPoints];

   for (int i = 0; i<iPoints; i++) {
      y[i] = pvec1[i].mean;
      ye[i] = pvec1[i].stdev;
      y2[i] = pvec2[i].mean;
      y2e[i] = pvec2[i].stdev;
      y3[i] = pvec3[i].mean;
      y3e[i] = pvec3[i].stdev;
      cout << i << "    " << pvec1[i].mean << "   " << pvec2[i].mean << "    " << pvec3[i].mean << endl;
   }
 
   TCanvas *c1 = new TCanvas("c1","c1");
   c1->cd();
   
   TGraphErrors *gr = new TGraphErrors(iPoints, x, y, xe, ye);
   TGraphErrors *gr2 = new TGraphErrors(iPoints, x, y2, xe, y2e);
   TGraphErrors *gr3 = new TGraphErrors(iPoints, x, y3, xe, y3e);
 
   gr2->SetTitle("Benchmarking synthetic non-hybrid data");
   gr2->GetXaxis()->SetTitle("number of events");
   gr2->GetYaxis()->SetTitle("Time (s)");
   gr2->GetYaxis()->SetRangeUser(0, 3800);
   gr2->Draw("ap");
   gr->Draw("p same");
   gr3->Draw("p same");
 
   gr2->Fit("pol1");
   TF1 *f2 = gr2->GetFunction("pol1");
   f2->SetLineWidth(2);
   f2->SetLineColor(kRed);

   gr->Fit("pol1");
   TF1 *f1 = gr->GetFunction("pol1");
   f1->SetLineWidth(2);
   f1->SetLineColor(kBlue);
   //f1->SetParNames("overhead time", "time per event");
   //gStyle->SetOptFit(1111);
   
   gr3->Fit("pol1");
   TF1 *f3 = gr3->GetFunction("pol1");
   f3->SetLineWidth(2);
   f3->SetLineColor(kBlack);
   
   TLegend *legend = new TLegend(0.25,0.65,0.46,0.82);
   //legend->SetHeader("The Legend Title");
   //legend->SetTextSize(0.05);
   //legend->SetBorderSize(0);
   legend->AddEntry(f1,"1x1OF only: slope=0.656","l");
   legend->AddEntry(f2,"1x2OF only: slope = 1.997","l");
   legend->AddEntry(f3,"1x1 + 1x2 + NxM: slope = 3.229","l");
   legend->Draw();

   c1->SaveAs("delete.pdf");
}

void bmCalc() {

   std::vector<plot_t> pvec1, pvec2, pvec3;

   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221203_171930.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_001701.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_002540.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_003606.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_004007.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_004216.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_004447.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_004634.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_004832.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_005049.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_005305.txt"));
   pvec1.push_back(calcFile("./output_1x1only/CPU_time_51221204_005539.txt"));

   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221203_171930.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_001701.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_002540.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_003606.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_004007.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_004216.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_004447.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_004634.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_004832.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_005049.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_005305.txt"));
   pvec2.push_back(calcFile("./output_1x1and1x2/CPU_time_51221204_005539.txt"));


   pvec3.push_back(calcFile("./output/CPU_time_51221203_171930.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_001701.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_002540.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_003606.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_004007.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_004216.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_004447.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_004634.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_004832.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_005049.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_005305.txt"));
   pvec3.push_back(calcFile("./output/CPU_time_51221204_005539.txt"));

   linear_fit_eventno(pvec1, pvec2, pvec3);

}

