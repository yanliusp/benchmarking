/// this file copies the TDirectory to another TDirectory in the same ROOT file
/// adapted from ROOT tutorials: copyFiles.C
/// https://root.cern.ch/root/html522/tutorials/io/copyFiles.C.html

#include "TROOT.h"
#include "TKey.h"
#include "TFile.h"
#include "TSystem.h"
#include "TTree.h"

void CopyDir(TDirectory * source) {
	source->ls();
	TDirectory *savedir = gDirectory;
	TDirectory *adir = savedir->mkdir("zip1");
	//TDirectory *source = (TDirectory*)file->Get("zip2");
	adir->cd();
	TKey *key;
	TIter nextkey(source->GetListOfKeys());
	while ((key = (TKey*)nextkey())) {
		const char *classname = key->GetClassName();
		TClass *cl = gROOT->GetClass(classname);
		if (!cl) continue;
		if (cl->InheritsFrom("TDirectory")) {
			source->cd(key->GetName());
			TDirectory *subdir = gDirectory;
			adir->cd();
			CopyDir(subdir);
			adir->cd();
		} else if (cl->InheritsFrom("TTree")) {
			TTree *T = (TTree*)source->Get(key->GetName());
			adir->cd();
			TTree *newT = T->CloneTree(-1,"fast");
			newT->Write();
		} else {
			source->cd();
			TObject *obj = key->ReadObj();
			adir->cd();
			obj->Write();
			delete obj;
		}
	}

	adir->SaveSelf(kTRUE);

}

void writeTemplate() {
	TFile * file = new TFile("/sdf/home/y/yanliu/software/cdmsbats_config/PulseTemplates/files/Z2_SLAC-R85-G133_Templates_Mod_2T_copy.root", "UPDATE");
	TDirectory *source = (TDirectory*)file->Get("zip2");
	CopyDir(source);
	delete source;
	delete file;

}
